import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Ships from './components/ships'
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ships: []
    };
  }

   maxValue = function name(value, numbers) {
   numbers.sort(function(a, b){return a - b});
   document.getElementById("max").innerHTML = numbers;
   document.write(numbers[numbers.length-1]);
  }

  componentDidMount() {
    fetch('https://swapi.co/api/starships/')
    .then(res => res.json())
    .then((data) => {
      data.results.sort((a, b) => a.crew - b.crew); 
      var lastMast = data.results.length ? data.results[data.results.length - 1] : null;

      if(lastMast) {
        data.results.forEach(element => {
          element.isMax = element.crew === lastMast.crew;
        });
      } 

      this.setState({ ships: data.results.filter(function(a, b){return parseInt(a.crew) > 10}) })
    })
    .catch(console.log)
  }

  render() {
    return (
      this.state.ships?
    <Ships ships={this.state.ships}/> : null
    )
  }
}

export default App;
