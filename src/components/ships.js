  import React from 'react'
  import Ship from './ship'

  const Ships = ({ ships }) => {
    return (
    <div>
      <center><h1>Ships</h1></center>
      <div class = "flex-container">
       
        {ships.map((ship) => (
           <Ship ship={ship}/>
        ))}
      </div>
    </div>
    )
  };

  export default Ships