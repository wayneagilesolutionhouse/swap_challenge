import React from 'react'
class Ship extends React.Component {

  render() {
    return (
        <div class="card col-md-3 card-layout">
        <div class="card-body">
          <h5 class="card-title">{this.props.ship.name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">{this.props.ship.model}</h6>
          <div class="img-container col-md-12">
            <img width="30px" 
            style={{ display: this.props.ship.isMax ? "block" : "none",  marginLeft: "80%" }}
            src={require(`../star.jpg`)}
            alt="star" />
          </div>
          <hr></hr>
          <div>
            <label class="card-text px-2 col-md-4">{this.props.ship.crew}</label>
            <label class="card-text px-2 col-md-4">{this.props.ship.passengers}</label>
            <label class="card-text px-2 col-md-4">{this.props.ship.films.length}</label>
          </div>
          <div>
            <label class="card-text px-2 col-md-4">Crew</label>
            <label class="card-text px-2 col-md-4">Passenger</label>
            <label class="card-text px-2 col-md-4">Film</label>
          </div>
        </div>
        </div>
    )
  }
}

export default Ship
